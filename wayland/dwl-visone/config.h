/* Taken from https://github.com/djpohly/dwl/issues/466 */
#define COLOR(hex)    { ((hex >> 24) & 0xFF) / 255.0f, \
                        ((hex >> 16) & 0xFF) / 255.0f, \
                        ((hex >> 8) & 0xFF) / 255.0f, \
                        (hex & 0xFF) / 255.0f }
/* appearance */
static const int sloppyfocus               = 1;  /* focus follows mouse */
static const int bypass_surface_visibility = 0;  /* 1 means idle inhibitors will disable idle tracking even if it's surface isn't visible  */
static const unsigned int borderpx         = 4;  /* border pixel of windows */
static const int smartgaps                 = 0;  /* 1 means no outer gap when there is only one window */
static int gaps                            = 1;  /* 1 means gaps between windows are added */
static const unsigned int gappx            = 10; /* gap pixel between windows */
static const float rootcolor[]             = COLOR(0x222222ff);
static const float bordercolor[]           = COLOR(0x81A1C1ff);
static const float focuscolor[]            = COLOR(0xB48EADff);
static const float urgentcolor[]           = COLOR(0xBF616Aff);
/* This conforms to the xdg-protocol. Set the alpha to zero to restore the old behavior */
static const float fullscreen_bg[]         = {0.1f, 0.1f, 0.1f, 1.0f}; /* You can also use glsl colors */

/* Autostart */
static const char *const autostart[] = {
        	"foot", "--server", NULL,
		"swaybg", "-i", "/media/datos/repos/wallpapers/linux/007.jpg", NULL,
		NULL /* terminate */
};

/* tagging - TAGCOUNT must be no greater than 31 */
#define TAGCOUNT (5)

/* logging */
static int log_level = WLR_ERROR;

/* NOTE: ALWAYS keep a rule declared even if you don't use rules (e.g leave at least one example) */
static const Rule rules[] = {
	/* app_id             title       tags mask     isfloating  isterm  noswallow  monitor */
	/* examples: */
	{ "chromium-devel",  NULL,       1 << 2,       0,          0,      0,         -1 }, /* Start on ONLY tag "9" */
	{ "firefox",  NULL,       1 << 2,       0,          0,      0,         -1 }, /* Start on ONLY tag "9" */
	{ "chrome-dkoiajbhajakglgcmkbhmcphnoiempab-Default",  NULL,       1 << 1,       0,          0,      0,         -1 }, /* Start on ONLY tag "9" */
	{ "chrome-ejhkdoiecgkmdpomoahkdihbcldkgjci-Default",  NULL,       1 << 1,       0,          0,      0,         -1 }, /* Start on ONLY tag "9" */
	{ "mpv",  NULL,       1 << 0,       0,          0,      0,         -1 }, /* Start on ONLY tag "9" */
	{ "footclient",             NULL,       0,            0,          1,      1,         -1 }, /* make foot swallow clients that are not foot */
	{ "foot",             NULL,       0,            0,          1,      1,         -1 }, /* make foot swallow clients that are not foot */
	{ "qemu-system-x86_64",             NULL,       1 << 4,            1,          1,      1,         -1 }, /* make foot swallow clients that are not foot */
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors */
/* (x=-1, y=-1) is reserved as an "autoconfigure" monitor position indicator
 * WARNING: negative values other than (-1, -1) cause problems with Xwayland clients
 * https://gitlab.freedesktop.org/xorg/xserver/-/issues/899
*/
/* NOTE: ALWAYS add a fallback rule, even if you are completely sure it won't be used */
/* monitors */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect                x    y */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
	*/
	/* defaults */
	{ NULL,       0.50, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
	/*.options = "caps:escape",*/
	.layout = "es",
	.options = NULL,
};

static const int repeat_rate = 80;
static const int repeat_delay = 300;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;
/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;

/* You can choose between:
LIBINPUT_CONFIG_CLICK_METHOD_NONE
LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS
LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER
*/
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER;

/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 1.0;
/* You can choose between:
LIBINPUT_CONFIG_TAP_MAP_LRM -- 1/2/3 finger tap maps to left/right/middle
LIBINPUT_CONFIG_TAP_MAP_LMR -- 1/2/3 finger tap maps to left/middle/right
*/
static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LRM;

/* If you want to use the windows key for MODKEY, use WLR_MODIFIER_LOGO */
#define MODKEY WLR_MODIFIER_LOGO

#define TAGKEYS(KEY,SKEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,SKEY,toggletag, {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[] = { "footclient", NULL };
static const char *termfootcmd[] = { "foot", NULL };
static const char *menucmd[] = { "launcher", NULL };
static const char *nnncmd[] = { "footclient", "-a", "nnn" "-e", "nnn", NULL };
static const char *pulsecmd[] = { "footclient", "-a", "pulse", "-e", "pulsemixer", NULL };
/*static const char *browsercmd[] = { "chromium-wayland", NULL };*/
static const char *browsercmd[] = { "firefox", NULL };
static const char *matrixcmd[] = { "vi-wapp", "-c", NULL };
static const char *mastodoncmd[] = { "vi-wapp", "-m", NULL };
static const char *powercmd[] = { "vi-session", NULL };
static const char *dpmscmd[] = { "vi-dpms", NULL };
static const char *grimscmd[] = { "vi-grim", "-s", NULL };
static const char *grimacmd[] = { "vi-grim", "-a", NULL };
static const char *rsscmd[] = { "vi-rss", "-m", NULL };
static const char *tekacmd[] = { "vi-teka", "-m", NULL };
static const char *upvolcmd[]    = { "pulse-vol", "+", NULL };
static const char *downvolcmd[]  = { "pulse-vol", "-", NULL };
static const char *mutevolcmd[]  = { "pulse-vol", "m", NULL };
static const char *brightupcmd[]  = { "bright", "+", NULL };
static const char *brightdowncmd[]  = { "bright", "-", NULL };

static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
	{ MODKEY,                    XKB_KEY_d,          spawn,          {.v = menucmd} },
	{ MODKEY,		     XKB_KEY_Return,     spawn,          {.v = termcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Return,	 spawn,          {.v = termfootcmd} },
	{ MODKEY,		     XKB_KEY_n,		 spawn,          {.v = nnncmd} },
	{ MODKEY,		     XKB_KEY_p,		 spawn,          {.v = pulsecmd} },
	{ MODKEY,		     XKB_KEY_b,		 spawn,          {.v = browsercmd} },
	{ MODKEY,		     XKB_KEY_t,		 spawn,          {.v = matrixcmd} },
	{ MODKEY,		     XKB_KEY_m,		 spawn,          {.v = mastodoncmd} },
	{ MODKEY,		     XKB_KEY_s,		 spawn,          {.v = powercmd} },
	{ MODKEY,		     XKB_KEY_Print,	 spawn,          {.v = grimscmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Print,	 spawn,          {.v = grimacmd} },
	{ WLR_MODIFIER_ALT,	     XKB_KEY_a,		 spawn,          {.v = dpmscmd} },
	{ WLR_MODIFIER_ALT,	     XKB_KEY_s,		 spawn,          {.v = rsscmd} },
	{ WLR_MODIFIER_ALT,	     XKB_KEY_t,		 spawn,          {.v = tekacmd} },
	{ MODKEY,                    XKB_KEY_Right,      focusstack,     {.i = +1} },
	{ MODKEY,                    XKB_KEY_Left,       focusstack,     {.i = -1} },
	{ MODKEY,                    XKB_KEY_Down,       incnmaster,     {.i = +1} },
	{ MODKEY,                    XKB_KEY_Up,         incnmaster,     {.i = -1} },
	{ MODKEY,                    XKB_KEY_h,          setmfact,       {.f = -0.05f} },
	{ MODKEY,                    XKB_KEY_l,          setmfact,       {.f = +0.05f} },
	{ WLR_MODIFIER_CTRL,	     XKB_KEY_Left,       viewnextocctag, {.i = -1} },
	{ WLR_MODIFIER_CTRL,	     XKB_KEY_Right,      viewnextocctag, {.i = +1} },
	{ MODKEY,                    XKB_KEY_g,          togglegaps,     {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_z,		 zoom,           {0} },
	{ MODKEY,		     XKB_KEY_q,          killclient,     {0} },
	{ MODKEY,                    XKB_KEY_7,          setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                    XKB_KEY_8,          setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                    XKB_KEY_9,          setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                    XKB_KEY_space,      setlayout,      {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_space,      togglefloating, {0} },
	{ MODKEY,                    XKB_KEY_f,          togglefullscreen, {0} },
	{ MODKEY,                    XKB_KEY_v,          view,           {.ui = ~0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_parenright, tag,            {.ui = ~0} },
	{ MODKEY,                    XKB_KEY_comma,      focusmon,       {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    XKB_KEY_period,     focusmon,       {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_less,       tagmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_greater,    tagmon,         {.i = WLR_DIRECTION_RIGHT} },
	TAGKEYS(          XKB_KEY_1, XKB_KEY_exclam,                     0),
	TAGKEYS(          XKB_KEY_2, XKB_KEY_quotedbl,                   1),
	TAGKEYS(          XKB_KEY_3, XKB_KEY_periodcentered,             2),
	TAGKEYS(          XKB_KEY_4, XKB_KEY_dollar,                     3),
	TAGKEYS(          XKB_KEY_5, XKB_KEY_percent,                    4),
	TAGKEYS(          XKB_KEY_6, XKB_KEY_ampersand,                  5),
	TAGKEYS(          XKB_KEY_7, XKB_KEY_slash,                      6),
	TAGKEYS(          XKB_KEY_8, XKB_KEY_parenleft,                  7),
	TAGKEYS(          XKB_KEY_9, XKB_KEY_parenright,                 8),
	TAGKEYS(          XKB_KEY_0, XKB_KEY_equal,                      9),
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Q,          quit,           {0} },

				    /* multimedia control keys */
	{ 0,                       XKB_KEY_XF86AudioRaiseVolume,  spawn,  {.v = upvolcmd } },
	{ 0,                       XKB_KEY_XF86AudioLowerVolume,  spawn,  {.v = downvolcmd } },
	{ 0,                       XKB_KEY_XF86AudioMute,         spawn,  {.v = mutevolcmd } },
	{ 0,                       XKB_KEY_XF86MonBrightnessUp,   spawn,  {.v = brightupcmd } },
	{ 0,                       XKB_KEY_XF86MonBrightnessDown, spawn,  {.v = brightdowncmd } },


	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_q, quit, {0} },
	/* Ctrl-Alt-Fx is used to switch to another VT, if you don't know what a VT is
	 * do not remove them.
	 */
#define CHVT(n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
