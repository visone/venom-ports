From f923b19fd85039a2b0e908391074872334646d51 Mon Sep 17 00:00:00 2001
From: Natanael Copa <ncopa@alpinelinux.org>
Date: Wed, 15 Jan 2025 15:48:04 +0100
Subject: [PATCH] Fix use-after-free in generator

full_fname() will free the return value in the next call so we need to
duplicate it before passing it to rsyserr.

Fixes: https://github.com/RsyncProject/rsync/issues/704
---
 generator.c | 6 +++++-
 1 file changed, 5 insertions(+), 1 deletion(-)

diff --git a/generator.c b/generator.c
index 3f13bb95..b56fa569 100644
--- a/generator.c
+++ b/generator.c
@@ -2041,8 +2041,12 @@ int atomic_create(struct file_struct *file, char *fname, const char *slnk, const
 
 	if (!skip_atomic) {
 		if (do_rename(tmpname, fname) < 0) {
+			char *full_tmpname = strdup(full_fname(tmpname));
+			if (full_tmpname == NULL)
+				out_of_memory("atomic_create");
 			rsyserr(FERROR_XFER, errno, "rename %s -> \"%s\" failed",
-				full_fname(tmpname), full_fname(fname));
+				full_tmpname, full_fname(fname));
+			free(full_tmpname);
 			do_unlink(tmpname);
 			return 0;
 		}

From efb85fd8db9e8f74eb3ab91ebf44f6ed35e3da5b Mon Sep 17 00:00:00 2001
From: Natanael Copa <ncopa@alpinelinux.org>
Date: Wed, 15 Jan 2025 15:10:24 +0100
Subject: [PATCH] Fix FLAG_GOT_DIR_FLIST collission with FLAG_HLINKED

fixes commit 688f5c379a43 (Refuse a duplicate dirlist.)

Fixes: https://github.com/RsyncProject/rsync/issues/702
Fixes: https://github.com/RsyncProject/rsync/issues/697
---
 rsync.h | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/rsync.h b/rsync.h
index 9be1297b..479ac484 100644
--- a/rsync.h
+++ b/rsync.h
@@ -84,7 +84,6 @@
 #define FLAG_DUPLICATE (1<<4)	/* sender */
 #define FLAG_MISSING_DIR (1<<4)	/* generator */
 #define FLAG_HLINKED (1<<5)	/* receiver/generator (checked on all types) */
-#define FLAG_GOT_DIR_FLIST (1<<5)/* sender/receiver/generator - dir_flist only */
 #define FLAG_HLINK_FIRST (1<<6)	/* receiver/generator (w/FLAG_HLINKED) */
 #define FLAG_IMPLIED_DIR (1<<6)	/* sender/receiver/generator (dirs only) */
 #define FLAG_HLINK_LAST (1<<7)	/* receiver/generator */
@@ -93,6 +92,7 @@
 #define FLAG_SKIP_GROUP (1<<10)	/* receiver/generator */
 #define FLAG_TIME_FAILED (1<<11)/* generator */
 #define FLAG_MOD_NSEC (1<<12)	/* sender/receiver/generator */
+#define FLAG_GOT_DIR_FLIST (1<<13)/* sender/receiver/generator - dir_flist only */
 
 /* These flags are passed to functions but not stored. */


From 10a6b1ecd76540e940f61d7750ae887676d2af2d Mon Sep 17 00:00:00 2001
From: Rodrigo OSORIO <rodrigo.osorio@softathome.com>
Date: Wed, 15 Jan 2025 10:32:48 +0100
Subject: [PATCH] Test send a single directory with -H enabled

Ensure this still working after 3.4.0 breakage

https://github.com/RsyncProject/rsync/issues/702
---
 testsuite/hardlinks.test | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/testsuite/hardlinks.test b/testsuite/hardlinks.test
index af2ea4088..68fd2701c 100644
--- a/testsuite/hardlinks.test
+++ b/testsuite/hardlinks.test
@@ -77,5 +77,11 @@ rm -rf "$todir"
 $RSYNC -aHivv --debug=HLINK5 "$name1" "$todir/"
 diff $diffopt "$name1" "$todir" || test_fail "solo copy of name1 failed"
 
+# Make sure there's nothing wrong with sending a single directory with -H
+# enabled (this has broken in 3.4.0 so far, so we need this test).
+rm -rf "$fromdir" "$todir"
+makepath "$fromdir/sym" "$todir"
+checkit "$RSYNC -aH '$fromdir/sym' '$todir'" "$fromdir" "$todir"
+
 # The script would have aborted on error, so getting here means we've won.
 exit 0
