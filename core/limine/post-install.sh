#! /bin/sh

# update  efi file
printf "Upgrading efi file.....\n"
[ ! -d /boot/EFI/BOOT ] && mkdir -p /boot/EFI/BOOT
install -Dm755  /usr/share/limine/BOOTX64.EFI -t /boot/EFI/BOOT/
