#!/bin/sh

msg() {
printf "%s\n" "$@"
}

kernver=$(find /lib/modules -maxdepth 1 -type d -iname '*-linux-lts' -printf "%f\n" )

msg "Removing old kernel..."

# removing other venom's kernel
for i in /lib/modules/*; do
	[ -d $i ] || continue
	case ${i##*/} in
		$kernver) continue;;
		*-linux-lts)
			[ -d $i/build/include ] && continue
			msg "post-install: removing kernel ${i##*/}"
			rm -fr $i;;
	esac
done

if [ $(command -v dracut) ]
    then
	msg "dracut: generating initramfs for kernel $kernver..."
	dracut --quiet --strip --aggressive-strip --force --kver $kernver /boot/initrd-linux-lts.img
    else
	msg "mkinitramfs: generating initramfs for kernel $kernver..."
	mkinitramfs -q -k $kernver -o /boot/initrd-linux-lts.img
fi

depmod $kernver

msg "Done!"
