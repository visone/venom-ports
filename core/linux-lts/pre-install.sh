#! /bin/sh

set -e

msg() {
    printf "%s\n" "$@"
}

dt="$(date +%d.%m.%y)"
read -r distro < /etc/hostname
case ${distro#*-} in
    station) tbdir="/media/datos/linux/tarballs/${distro%-*}/$dt" ;;
    laptop) tbdir="/media/linux/tarballs/${distro%-*}/$dt" ;;
esac
snapdir="/.snapshots"
name="${distro#*-}-$dt.zst"
[ -d $tbdir ] || ( mkdir -p $tbdir && chown -R visone:visone $tbdir )

svol() {
[ -f $tbdir/$2-$name ] && rm -f $tbdir/$2-$name
btrfs subv snapshot -r $1 $snapdir/$2-$dt
btrfs send $snapdir/$2-$dt | zstdmt -c > $tbdir/$2-$name
btrfs subv delete $snapdir/$2-$dt
}

home() {
svol /home home
}

boot() {
[ -f $tbdir/boot-$name ] && rm -f $tbdir/boot-$name
cd /boot
bsdtar -aczf $tbdir/boot-$name . && msg "Boot tarball created!"
}

root() {
svol / root
}

home
root
boot
