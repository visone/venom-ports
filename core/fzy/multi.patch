--- a/src/choices.c	2023-12-18 15:09:37.189026296 +0100
+++ b/src/choices.c	2023-12-18 15:12:00.871020567 +0100
@@ -265,13 +265,23 @@
 	choices_reset_search(c);
 
 	struct search_job *job = calloc(1, sizeof(struct search_job));
+	if (!job) {
+		fprintf(stderr, "Error: Can't allocate memory\n");
+		abort();
+	}
+
 	job->search = search;
 	job->choices = c;
 	if (pthread_mutex_init(&job->lock, NULL) != 0) {
 		fprintf(stderr, "Error: pthread_mutex_init failed\n");
 		abort();
 	}
+
 	job->workers = calloc(c->worker_count, sizeof(struct worker));
+	if (!job->workers) {
+		fprintf(stderr, "Error: Can't allocate memory\n");
+		abort();
+	}
 
 	struct worker *workers = job->workers;
 	for (int i = c->worker_count - 1; i >= 0; i--) {


--- a/src/options.c	2023-12-18 15:13:16.624017547 +0100
+++ b/src/options.c	2023-12-18 15:20:21.901000590 +0100
@@ -12,7 +12,9 @@
     ""
     "Usage: fzy [OPTION]...\n"
     " -l, --lines=LINES        Specify how many lines of results to show (default 10)\n"
+    " -m, --multi              Enable multi-selection\n"
     " -p, --prompt=PROMPT      Input prompt (default '> ')\n"
+    " -P, --pad=NUM            Left pad list of matches NUM places (default 2)\n"
     " -q, --query=QUERY        Use QUERY as the initial search string\n"
     " -e, --show-matches=QUERY Output the sorted matches of QUERY\n"
     " -t, --tty=TTY            Specify file to use as TTY device (default /dev/tty)\n"
@@ -20,14 +22,14 @@
     " -0, --read-null          Read input delimited by ASCII NUL characters\n"
     " -j, --workers NUM        Use NUM workers for searching. (default is # of CPUs)\n"
     " -i, --show-info          Show selection info line\n"
-    " -h, --help     Display this help and exit\n"
-    " -v, --version  Output version information and exit\n";
+    " -h, --help               Display this help and exit\n"
+    " -v, --version            Output version information and exit\n";
 
 static void usage(const char *argv0) {
 	fprintf(stderr, usage_str, argv0);
 }
-
-static struct option longopts[] = {{"show-matches", required_argument, NULL, 'e'},
+static struct option longopts[] = {
+				   {"show-matches", required_argument, NULL, 'e'},
 				   {"query", required_argument, NULL, 'q'},
 				   {"lines", required_argument, NULL, 'l'},
 				   {"tty", required_argument, NULL, 't'},
@@ -39,7 +41,10 @@
 				   {"workers", required_argument, NULL, 'j'},
 				   {"show-info", no_argument, NULL, 'i'},
 				   {"help", no_argument, NULL, 'h'},
-				   {NULL, 0, NULL, 0}};
+				   {"pad", required_argument, NULL, 'P'},
+				   {"multi", no_argument, NULL, 'm'},
+				   {NULL, 0, NULL, 0}
+};
 
 void options_init(options_t *options) {
 	/* set defaults */
@@ -54,6 +59,8 @@
 	options->workers         = DEFAULT_WORKERS;
 	options->input_delimiter = '\n';
 	options->show_info       = DEFAULT_SHOW_INFO;
+	options->pad             = 2;
+	options->multi           = 0;
 }
 
 void options_parse(options_t *options, int argc, char *argv[]) {
@@ -63,7 +70,7 @@
 	while ((c = getopt_long(argc, argv, "vhs0e:q:l:t:p:j:i", longopts, NULL)) != -1) {
 		switch (c) {
 			case 'v':
-				printf("%s " VERSION " © 2014-2018 John Hawthorn\n", argv[0]);
+				//				printf("%s " VERSION " © 2014-2018 John Hawthorn\n", argv[0]);
 				exit(EXIT_SUCCESS);
 			case 's':
 				options->show_scores = 1;
@@ -71,6 +78,9 @@
 			case '0':
 				options->input_delimiter = '\0';
 				break;
+			case 'm':
+				options->multi = 1;
+				break;
 			case 'q':
 				options->init_search = optarg;
 				break;
@@ -93,6 +103,10 @@
 			case 'p':
 				options->prompt = optarg;
 				break;
+			case 'P':
+				if (optarg)
+					options->pad = atoi(optarg);
+				break;
 			case 'j':
 				if (sscanf(optarg, "%u", &options->workers) != 1) {
 					usage(argv[0]);
@@ -100,6 +114,8 @@
 				}
 				break;
 			case 'l': {
+				if (!optarg)
+					break;
 				int l;
 				if (!strcmp(optarg, "max")) {
 					l = INT_MAX;
@@ -114,7 +130,8 @@
 			case 'i':
 				options->show_info = 1;
 				break;
-			case 'h':
+			
+			case 'h': /* fallthrough */
 			default:
 				usage(argv[0]);
 				exit(EXIT_SUCCESS);


--- a/src/options.h	2023-12-18 15:20:58.168999143 +0100
+++ b/src/options.h	2023-12-18 15:22:02.192996591 +0100
@@ -13,6 +13,8 @@
 	unsigned int workers;
 	char input_delimiter;
 	int show_info;
+	int pad;
+	int multi;
 } options_t;
 
 void options_init(options_t *options);


--- a/src/tty_interface.c	2023-12-18 15:22:43.773994933 +0100
+++ b/src/tty_interface.c	2023-12-18 15:32:24.067971795 +0100
@@ -7,6 +7,120 @@
 #include "tty_interface.h"
 #include "../config.h"
 
+/* An array numbers to store the index of selected entries (choices) */
+static size_t *selection_marks = (size_t *)NULL;
+
+/* Create a size_t array big enough to hold all availalable choices,
+ * and initialize it to zero */
+static void
+init_selection_marks(tty_interface_t *state)
+{
+	if (state->options->multi == 0) {
+		return;
+	}
+
+	selection_marks = (size_t *)malloc(state->choices->available * sizeof(size_t));
+	for (size_t i = 0; i < state->choices->available; i++) {
+		selection_marks[i] = 0;
+	}
+}
+
+/* Return the amount of selected entries in the list of matches */
+static size_t
+count_selections(tty_interface_t *state)
+{
+	size_t c = 0;
+	for (size_t i = 0; i < state->choices->available; i++) {
+		if (selection_marks[i] == 1) {
+			c++;
+		}
+	}
+
+	return c;
+}
+
+/* Store current selected entries into an array to print each of them
+ * at exit. Return this array of strings if success and NULL in case
+ * of error or no selected entries */
+static char **
+save_selections(tty_interface_t *state)
+{
+	size_t i, c = 0;
+	size_t n = count_selections(state);
+	if (n == 0) {
+		return (char **)NULL;
+	}
+
+	char **s = (char **)malloc((n + 2) * sizeof(char *));
+
+	for (i = 0; i < state->choices->available; i++) {
+		if (selection_marks[i] != 1) {
+			continue;
+		}
+
+		const char *name = choices_get(state->choices, i);
+		if (!name) {
+			continue;
+		}
+
+		s[c] = (char *)malloc((strlen(name) + 1) * sizeof(char));
+		strcpy(s[c], name);
+		c++;
+
+		if (c == n) {
+			break;
+		}
+	}
+
+	if (c == 0) {
+		free(s);
+		return (char **)NULL;
+	}
+
+	s[c] = (char *)NULL;
+	return s;
+}
+
+/* Free the array of selected entries generated by save_selections() */
+static void
+free_selections(char **s)
+{
+	size_t i;
+	for (i = 0; s[i]; i++) {
+		free(s[i]);
+	}
+	free(s);
+}
+
+/* Print the list of selected entries generated by save_selections() */
+static void
+print_selections(char **s)
+{
+	size_t i;
+	for (i = 0; s[i]; i++) {
+		printf("%s\n", s[i]);
+	}
+}
+
+/* Free the size_t array generated by init_selection_marks() */
+static void
+free_selection_marks(tty_interface_t *state)
+{
+	if (state->options->multi == 1) {
+		free(selection_marks);
+		selection_marks = (size_t *)NULL;
+	}
+}
+
+/* Set to 1 the index of the current/selected entry in the selection_marks
+ * array. If this entry is already marked/selected, set it to zero */
+static void
+action_select(tty_interface_t *state)
+{
+	size_t s = selection_marks[state->choices->selection];
+	selection_marks[state->choices->selection] = s == 1 ? 0 : 1;
+}
+
 static int isprint_unicode(char c) {
 	return isprint(c) || c & (1 << 7);
 }
@@ -18,8 +132,8 @@
 static void clear(tty_interface_t *state) {
 	tty_t *tty = state->tty;
 
-	tty_setcol(tty, 0);
-	size_t line = 0;
+		tty_setcol(tty, state->options->pad);
+		size_t line = 0;
 	while (line++ < state->options->num_lines + (state->options->show_info ? 1 : 0)) {
 		tty_newline(tty);
 	}
@@ -91,7 +205,7 @@
 		}
 	}
 
-	tty_setcol(tty, 0);
+	tty_setcol(tty, options->pad);
 	tty_printf(tty, "%s%s", options->prompt, state->search);
 	tty_clearline(tty);
 
@@ -105,6 +219,11 @@
 		tty_clearline(tty);
 		const char *choice = choices_get(choices, i);
 		if (choice) {
+			if (options->multi == 1 && selection_marks[i] == 1) {
+				tty_printf(tty, "%*c %s", options->pad - 1, '*', "");
+			} else {
+				tty_printf(tty, "%*s", options->pad, "");
+			}
 			draw_match(state, choice, i == choices->selection);
 		}
 	}
@@ -112,7 +231,7 @@
 	if (num_lines + options->show_info)
 		tty_moveup(tty, num_lines + options->show_info);
 
-	tty_setcol(tty, 0);
+	tty_setcol(tty, options->pad);
 	fputs(options->prompt, tty->fout);
 	for (size_t i = 0; i < state->cursor; i++)
 		fputc(state->search[i], tty->fout);
@@ -134,6 +253,22 @@
 static void action_emit(tty_interface_t *state) {
 	update_state(state);
 
+	if (state->options->multi == 1) {
+		char **s = save_selections(state);
+		free_selection_marks(state);
+
+		clear(state);
+		tty_close(state->tty);
+
+		if (s) {
+			print_selections(s);
+			free_selections(s);
+		}
+
+		state->exit = EXIT_SUCCESS;
+		return;
+	}
+
 	/* Reset the tty as close as possible to the previous state */
 	clear(state);
 
@@ -236,6 +371,12 @@
 }
 
 static void action_autocomplete(tty_interface_t *state) {
+	if (state->options->multi == 1) {
+		    action_select(state);
+		    action_next(state);
+		    return;
+	}
+
 	update_state(state);
 	const char *current_selection = choices_get(state->choices, state->choices->selection);
 	if (current_selection) {
@@ -372,6 +513,7 @@
 }
 
 int tty_interface_run(tty_interface_t *state) {
+	init_selection_marks(state);
 	draw(state);
 
 	for (;;) {
@@ -384,8 +526,10 @@
 			char s[2] = {tty_getchar(state->tty), '\0'};
 			handle_input(state, s, 0);
 
-			if (state->exit >= 0)
+			if (state->exit >= 0) {
+				free_selection_marks(state);
 				return state->exit;
+			}
 
 			draw(state);
 		} while (tty_input_ready(state->tty, state->ambiguous_key_pending ? KEYTIMEOUT : 0, 0));
@@ -394,8 +538,10 @@
 			char s[1] = "";
 			handle_input(state, s, 1);
 
-			if (state->exit >= 0)
+			if (state->exit >= 0) {
+				free_selection_marks(state);
 				return state->exit;
+			}
 		}
 
 		update_state(state);
