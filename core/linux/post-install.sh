#!/bin/sh

msg() {
printf "%s\n" "$@"
}

kernver=$(find /lib/modules -maxdepth 1 -type d -iname '*-linux' -printf "%f\n" )

msg "Removing old kernel..."

# removing other venom's kernel
for i in /lib/modules/*; do
	[ -d $i ] || continue
	case ${i##*/} in
		$kernver) continue;;
		*-linux)
			[ -d $i/build/include ] && continue
			msg "post-install: removing kernel ${i##*/}"
			rm -rf $i;;
	esac
done


if [ $(command -v dracut) ]
    then
	msg "dracut: generating initramfs for kernel $kernver..."
	dracut --quiet --strip --aggressive-strip --force --kver $kernver /boot/initrd-linux.img
    else
	msg "mkinitramfs: generating initramfs for kernel $kernver..."
	mkinitramfs -q -k $kernver -o /boot/initrd-linux.img
fi

depmod $kernver

msg "Done!"
