#!/bin/sh

udevadm control --reload-rules && udevadm trigger

printf "%s\n" 
"Set permissions and grpoups like this:
# gpasswd -a <username> kvm
# gpasswd -a <username> vhost
# gpasswd -a <username> usb
# ls -1 /usr/bin/qemu-system-* | xargs -n1 setcap CAP_NET_ADMIN=ep
# setcap CAP_NET_ADMIN=ep /usr/bin/nemu
"
