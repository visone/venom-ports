#!/bin/sh

. /etc/rc.subr
NAME="ly service"
PROG=/usr/bin/ly-dm
PID=/run/ly/pid

case $1 in
start)
	msg "Starting $NAME..."
        start_daemon -p $PID  $PROG  $OPTS
        ;;
stop)
	msg "Stopping $NAME..."
        stop_daemon -p $PID $PROG
        ;;
restart)
        $0 stop
	sleep 1
        $0 start
        ;;
status)
        status_daemon $PROG
        e=$?
        case $e in
        0) echo "$PROG is running with pid $(cat $PID)" ;;
        1) echo "$PROG is not running but the pid file $PID exists" ;;
        3) echo "$PROG is not running" ;;
        4) echo "Unable to determine the program status" ;;
        esac
        exit $e
        ;;
*)
        echo "usage: $0 [start|stop|restart|status]"
        ;;
esac
