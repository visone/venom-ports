/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = ":mononoki Nerd Font Mono:size=12";
static const char *fonts[] = {
	font,
	"mononoki Nerd Font Mono:size=14",
	"FantasqueSansMono Nerd Font Mono:size=14",
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#B48EAD", "#2E3440" },
	[SchemeSel] = { "#2E3440", "#8FBCBB" },
	[SchemeOut] = { "#2E3440", "#BF616A" },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 4;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 3;
