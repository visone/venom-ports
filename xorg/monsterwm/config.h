/* see LICENSE for copyright and license */

#ifndef CONFIG_H
#define CONFIG_H

/** modifiers **/
#define MOD1            Mod1Mask    /* ALT key */
#define MOD4            Mod4Mask    /* Super/Windows key */
#define CONTROL         ControlMask /* Control key */
#define SHIFT           ShiftMask   /* Shift key */

/** generic settings **/
#define MASTER_SIZE     0.50
#define SHOW_PANEL      False      /* show panel by default on exec */
#define TOP_PANEL       True      /* False means panel is on bottom */
#define PANEL_HEIGHT    0        /* 0 for no space for panel, thus no panel */
#define DEFAULT_MODE    TILE      /* initial layout/mode: TILE MONOCLE BSTACK GRID FLOAT */
#define ATTACH_ASIDE    True      /* False means new window is master */
#define FOLLOW_WINDOW   False     /* follow the window when moved to a different desktop */
#define FOLLOW_MOUSE    True     /* focus the window the mouse just entered */
#define CLICK_TO_FOCUS  True      /* focus an unfocused window when clicked  */
#define FOCUS_BUTTON    Button3   /* mouse button to be used along with CLICK_TO_FOCUS */
#define BORDER_WIDTH    5         /* window border width */
#define FOCUS           "#B48EAD" /* focused window border color    */
#define UNFOCUS         "#A3BE8C" /* unfocused window border color  */
#define MINWSZ          50        /* minimum window size in pixels  */
#define DEFAULT_DESKTOP 0         /* the desktop to focus initially */
#define DESKTOPS        4         /* number of desktops - edit DESKTOPCHANGE keys to suit */
#define USELESSGAP      10         /* the size of the useless gap in pixels */

/**
 * open applications to specified desktop with specified mode.
 * if desktop is negative, then current is assumed
 */
static const AppRule rules[] = { \
    /*  class               desktop  follow  float */
    { "URxvt",                  -2,     False,  True },
    { "kermit",                 -2,     False,  True },
    { "brave-browser",           2,     False,  True  },
    { "mpv",                     0,     True,   True  },
};

/* helper for spawning shell commands */
#define SHCMD(cmd) {.com = (const char*[]){"/bin/sh", "-c", cmd, NULL}}

/**
 * custom commands
 * must always end with ', NULL };'
 */
/* dmenu */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-x", "760", "-y", "20", "-z", "400", "-p", "VisoneRun:",  NULL }; 
static const char *launchercmd[] = {"launcher", NULL };
static const char *fzfmenucmd[] = { "term-launcher",  "-m", NULL };

/* the st/urxvt terminal*/
static const char *scratchpadcmd[] = {"sctpad", "float", NULL }; 
static const char *termcmd[]  = {"sctpad", "scratchpad",  NULL };
static const char *urxcmd[] = {"sctpad", "rxvt", NULL };

/* Apps bindings */
static const char *browsercmd[]          = { "vi-brave", NULL }; 
static const char *tgcmd[]          = { "vi-tg", NULL }; 


/* Audio */
#include<X11/XF86keysym.h>

static const char *upvolcmd[]    = { "dwm-vol", "+", NULL };
static const char *downvolcmd[]  = { "dwm-vol", "-", NULL };
static const char *mutevolcmd[]  = { "dwm-vol", "m", NULL };


/* Brightness */

static const char *brightpcmd[] =  { "bright", "+", NULL };
static const char *brightmcmd[] =  { "bright", "-", NULL };


/* Visone Scripts*/
static const char *sessioncmd[]  = { "void-session", NULL };
static const char *fzfilmscmd[]  = { "term-launcher", "-f", NULL };
static const char *fztvshowcmd[] = { "term-launcher", "-t", NULL };
static const char *fzvarioscmd[] = { "term-launcher", "-v", NULL };
static const char *ssmcmd[]       = { "dwm-screenshoots", "-m", NULL };
static const char *sscmd[]       = { "dwm-screenshoots", "-s", NULL };








#define DESKTOPCHANGE(K,N) \
    {  MOD4,             K,              change_desktop, {.i = N}}, \
    {  MOD4|ShiftMask,   K,              client_to_desktop, {.i = N}},

/**
 * keyboard shortcuts
 */
static Key keys[] = {
    /* modifier          key            function           argument */

	{ MOD4,				XK_d,      spawn,          {.com = dmenucmd}}, // dmenu
	{ MOD1|SHIFT,		XK_d,      spawn,          {.com = fzfmenucmd}}, // fzf-menu
	{ MOD1,				XK_d, 	   spawn,  	   {.com = launchercmd}}, // launcher
	{ MOD4,            	XK_Return, spawn, 	   {.com = urxcmd}}, // urxvtc st/nnn
	{ MOD4|SHIFT,       XK_Return, spawn,  	   {.com = termcmd}}, // st/nnn tabbed
	{ MOD1,				XK_Return, spawn,  	   {.com = scratchpadcmd}}, // urxvtc float
	{ MOD4,  	      	XK_b,      spawn,  	   {.com = browsercmd}}, // browser
	{ MOD4,  	      	XK_t,      spawn,  	   {.com = tgcmd}}, // telegram
	{ 0,                XK_Print,  spawn,          {.com = sscmd}}, //desktop-screenshot
	{ MOD4,             XK_Print,  spawn,          {.com = ssmcmd}}, //menu-screenshot
	{ MOD4,           	XK_s,      spawn,          {.com = sessioncmd}}, //session

		/* Audio Bindings */

	{ 0, XF86XK_AudioMute,        	spawn,      {.com = mutevolcmd}}, // audio mute
	{ 0, XF86XK_AudioLowerVolume,   spawn,      {.com = downvolcmd}}, // audio down
	{ 0, XF86XK_AudioRaiseVolume,   spawn,      {.com = upvolcmd}}, // audio plus
	

		/* Brightness bindings */

	{ 0, XF86XK_MonBrightnessUp,	spawn,	{.com = brightpcmd}},
	{ 0, XF86XK_MonBrightnessDown,	spawn,	{.com = brightmcmd}},



		/* Scripts Alt + Key */

	{MOD1,				XK_1,	spawn,		{.com = fzfilmscmd   } },		
	{MOD1,				XK_2,   spawn,		{.com = fztvshowcmd   } },		
	{MOD1,				XK_3,   spawn,		{.com = fzvarioscmd   } },		
	

    {MOD4,              XK_z,          togglepanel,		{NULL}},
    {MOD4,              XK_a,	      focusurgent,		{NULL}},
    {MOD4,		        XK_q,         killclient,		{NULL}},
    {MOD4,              XK_Tab,        next_win,		{NULL}},
    {MOD4|SHIFT,        XK_Tab,        prev_win,		{NULL}},
    {MOD1,              XK_Left,       resize_master,	{.i = -10}}, /* decrease size in px */
    {MOD1,              XK_Right,      resize_master,	{.i = +10}}, /* increase size in px */
    {MOD1|SHIFT,        XK_Left,       resize_stack,	{.i = -10}}, /* shrink   size in px */
    {MOD1|SHIFT,        XK_Right,      resize_stack,	{.i = +10}}, /* grow     size in px */
    {CONTROL|SHIFT,     XK_Left,       rotate,			{.i = -1}},
    {CONTROL|SHIFT,     XK_Right,      rotate,			{.i = +1}},
    {MOD1|SHIFT,        XK_Up,         rotate_filled,	{.i = -1}},
    {MOD1|SHIFT,        XK_Down,       rotate_filled,	{.i = +1}},
    {MOD1,              XK_Tab,        last_desktop,	{NULL}},
    {MOD1,              XK_space,	   swap_master,		{NULL}},
    {CONTROL,		    XK_Up,         move_down,		{NULL}},
    {CONTROL,		    XK_Down,       move_up,			{NULL}},
    {CONTROL,		    XK_1,          switch_mode,		{.i = TILE}},
    {CONTROL,		    XK_2,          switch_mode,		{.i = MONOCLE}},
    {CONTROL,		    XK_3,          switch_mode,		{.i = BSTACK}},
    {CONTROL,		    XK_4,          switch_mode,		{.i = GRID}},
    {CONTROL,		    XK_5,          switch_mode,		{.i = FLOAT}},
    {MOD4|SHIFT,	    XK_r,          quit,			{.i = 0}}, /* quit with exit value 0 */
    {MOD4|SHIFT,        XK_q,          quit,			{.i = 1}}, /* quit with exit value 1 */
//  {MOD4|MOD1,         XK_Down,       moveresize,		{.v = (int []){   0,  25,   0,   0 }}}, /* move down  */
//  {MOD4|MOD1,         XK_Up,         moveresize,		{.v = (int []){   0, -25,   0,   0 }}}, /* move up    */
//  {MOD4|MOD1,         XK_Right,      moveresize,		{.v = (int []){  25,   0,   0,   0 }}}, /* move right */
//  {MOD4|MOD1,         XK_Left,       moveresize,		{.v = (int []){ -25,   0,   0,   0 }}}, /* move left  */
//  {MOD4|CONTROL,	    XK_Down,       moveresize,		{.v = (int []){   0,   0,   0,  25 }}}, /* height grow   */
//  {MOD4|CONTROL,	    XK_Up,         moveresize,		{.v = (int []){   0,   0,   0, -25 }}}, /* height shrink */
//  {MOD4|CONTROL,	    XK_Right       moveresize,		{.v = (int []){   0,   0,  25,   0 }}}, /* width grow    */
//  {MOD4|CONTROL,	    XK_Left,       moveresize,		{.v = (int []){   0,   0, -25,   0 }}}, /* width shrink  */

       DESKTOPCHANGE(    XK_1,                             0)
       DESKTOPCHANGE(    XK_2,                             1)
       DESKTOPCHANGE(    XK_3,                             2)
       DESKTOPCHANGE(    XK_4,                             3)
};

/**
 * mouse shortcuts
 */
static Button buttons[] = {
    {  MOD4,    Button1,     mousemotion,   {.i = MOVE}},
    {  MOD4,    Button3,     mousemotion,   {.i = RESIZE}},
    {  MOD4,    Button3,     spawn,         {.com = dmenucmd}},
};
#endif

/* vim: set expandtab ts=4 sts=4 sw=4 : */
